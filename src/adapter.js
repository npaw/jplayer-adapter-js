var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.JPlayer = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    if (!this.internalPlayer) return null
    return this.internalPlayer.status.currentTime
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    if (!this.internalPlayer) return null
    return this.internalPlayer.status.playbackRate
  },

  /** Override to return video duration */
  getDuration: function () {
    if (!this.internalPlayer) return null
    return this.internalPlayer.status.duration
  },

  /** Override to return rendition */
  getRendition: function () {
    return youbora.Util.buildRenditionString(this.internalPlayer.status.videoWidth, this.internalPlayer.status.videoHeight)
  },

  /** Override to return title */
  getTitle: function () {
    if (!this.internalPlayer) return null
    return this.internalPlayer.status.media.title
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return false
  },

  /** Override to return resource URL. */
  getResource: function () {
    if (!this.internalPlayer) return null
    return this.internalPlayer.status.src
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    if (!this.internalPlayer) return null
    return this.internalPlayer.version.script
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'jPlayer'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    var events = $.jPlayer.event
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player.bind(), [null,
      events.ready,
      events.setmedia,
      events.flashreset,
      events.resize,
      events.repeat,
      events.click,
      events.error,
      events.warning,
      events.loadstart,
      events.progress,
      events.suspend,
      events.abort,
      events.emptied,
      events.stalled,
      events.play,
      events.pause,
      events.loadedmetadata,
      events.loadedata,
      events.waiting,
      events.playing,
      events.canplay,
      events.canplaythrough,
      events.seeking,
      events.seeked,
      events.timeupdate,
      events.ended,
      events.ratechange,
      events.durationchange,
      events.volumechange
    ])

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false, 1200)

    this.internalPlayer = null
    // References
    this.references = []
    this.references[events.play] = this.playListener.bind(this)
    this.references[events.timeupdate] = this.timeupdateListener.bind(this)
    this.references[events.pause] = this.pauseListener.bind(this)
    this.references[events.playing] = this.playingListener.bind(this)
    this.references[events.error] = this.errorListener.bind(this)
    this.references[events.seeking] = this.seekingListener.bind(this)
    this.references[events.seeked] = this.seekedListener.bind(this)
    this.references[events.ended] = this.endedListener.bind(this)

    // Register listeners
    for (var key in this.references) {
      this.player.bind(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    this.monitor.stop()

    this.internalPlayer = null
    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.unbind(key, this.references[key])
      }
      this.references = []
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.setInternalPlayer()
    this.fireStart()
  },

  /** Listener for 'timeupdate' event. */
  timeupdateListener: function (e) {
    if (this.getPlayhead() > 0.15) {
      this.fireStart()
      if (!this.flags.isJoined) {
        this.fireJoin()
        this.monitor.skipNextTick()
      }
      this.lastPlayhead = this.internalPlayer.status.currentTime
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
    this.fireSeekEnd()
    if (this.getPlayhead() > 0) {
      this.fireJoin()
    }
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    this.fireError()
    this.fireStop()
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'seeked' event. */
  seekedListener: function (e) {
    if (!this.flags.isPaused)
      this.fireSeekEnd()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop({ playhead: this.lastPlayhead })
  },

  setInternalPlayer: function (e) {
    if (!this.internalPlayer) {
      this.internalPlayer = this.player.data('jPlayer')
    }
  }
})

module.exports = youbora.adapters.JPlayer
